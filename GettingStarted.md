Getting Started
===============
 
external Dependencies
--------------------
* [Buildsystem Tup](http://gittup.org/tup/)
* [The Lean Mean C++ Option Parser]( http://optionparser.sourceforge.net/ )
* Compiler ...

Build it 
---------------
call 'tup.exe init' in the root folder

call 'tup.exe[upd]' in the root folder

structure
---------

/test
	tests the final executeable
	
/src 
	sourceode for the final executeable
	
/lib 
	source of the main functionality
	
/ext
	folder for external dependencies	