/**
 * \file main.cpp
 * \date 17.12.2016
 * \author: saell
 */

#include <stdio.h>
#include <iostream>
#include "optionparser.h"
#include "debug.h"

#include "LocStat.h"

namespace
{

const char* ERROR_OUTPUT_NO_FILE_TO_ANALYZE_GIVEN = "No File to analyse given\n";
const char* ERROR_OUTPUT_FILE_NOT_FOUND = "File given not found. Does it Exist?\n";

}

enum  optionIndex { UNKNOWN, HELP,VERBOSE, FILEPATH };
const option::Descriptor usage[] =
{
  {UNKNOWN , 0,"" , ""       , option::Arg::None, "\n"
                                                  "USAGE: locstat.exe [options] sourcefile\n\n"
                                                  "Options:" },
  {HELP    , 0,"h", "help"   , option::Arg::None, "  --help, -h    \tPrint usage and exit." },
  {VERBOSE , 0,"v", "verbose", option::Arg::None, "  --verbose, -v \tPrint File content." },
  {FILEPATH, 0,"" , "file"   , option::Arg::None, "  --file        \tPath to file to analyze." },
  {UNKNOWN , 0,"" , ""       , option::Arg::None, "\nExamples:\n"
                                                  " locstat.exe ./path/toFile.cpp\n"
                                                  " locstat.exe --file ./path/toFile.cpp" },
{ 0, 0, 0, 0, 0, 0 } };

int main(int argc, char* argv[])
{
    argc -= (argc > 0);
    argv += (argc > 0); // skip program name argv[0] if present
    option::Stats stats( usage, argc, argv );
    option::Option options[stats.options_max], buffer[stats.buffer_max];
    option::Parser parse( usage, argc, argv, options, buffer );

    if ( parse.error( ) )
        return 1;

    if ( options[HELP] || argc == 0 )
    {
        ERROR_Output( ERROR_OUTPUT_NO_FILE_TO_ANALYZE_GIVEN );
        option::printUsage( std::cout, usage );
        return 0;
    }

    for ( option::Option* opt = options[UNKNOWN]; opt; opt = opt->next( ) )
    {
        std::cout << "Unknown option: " << opt->name << std::endl;
    }

    for ( option::Option* opt = options[FILEPATH]; opt; opt = opt->next( ) )
    {
        std::cout << "paths option: " << opt->name << std::endl;
    }

    for ( option::Option* opt = options[VERBOSE]; opt; opt = opt->next( ) )
    {
        std::cout << "verbose option: " << opt->name << std::endl;
        //todo verbosity evaluation
        //todo adapt out put to give more information to user
    }

    if ( parse.nonOptionsCount() > 3 )
    {
        ERROR_Output( "Too many options:\n" );
        for ( int i = 0; i < parse.nonOptionsCount( ); ++i )
        {
            std::cerr << "Non-option #" << i << ": " << parse.nonOption( i ) << std::endl;
        }

        option::printUsage( std::cout, usage );
        return -1;
    }
    else if ( parse.nonOptionsCount() == 0 )
    {
        ERROR_Output( ERROR_OUTPUT_NO_FILE_TO_ANALYZE_GIVEN );
        option::printUsage( std::cout, usage );
        return -1;
    }

    LocStat mlocstat = LocStat( );

    //Print Header of Table
    std::cout << std::endl << "file, total, code, commens, empty" << std::endl;

    //go through all options given that are not known
    const char* pathToFile;
    for (int index = 0; index < parse.nonOptionsCount(); index++)
    {
        pathToFile = parse.nonOption( index );
        if ( !pathToFile )
        {
            ERROR_Output( ERROR_OUTPUT_NO_FILE_TO_ANALYZE_GIVEN );
            return -1;
        }

        int errorcode = mlocstat.analyseFile( pathToFile );
        switch ( errorcode )
        {
            case 0:
                std::cout << pathToFile << ", " << mlocstat.lines_All << ", " << mlocstat.lines_Code << ", "
                        << mlocstat.lines_Comment << ", " << mlocstat.lines_Empty << std::endl;
                break;
            case -1:
                ERROR_Output( "Can't analyse File:<%s>. Does it exist?\n", pathToFile );
                break;
            default:
                ERROR_Output( "Unkown Error %d\n", errorcode );
                break;
        }
    }

    return 0;
}
