/*
 * debug.h
 *
 *  Created on: 06.04.2017
 *      Author: Sascha
 */

#ifndef SRC_DEBUG_H_
#define SRC_DEBUG_H_

 
#define ERROR_Output( ... )        \
{                                  \
    fprintf( stderr, __VA_ARGS__ );\
    fprintf( stdout, __VA_ARGS__ );\
}                                       


#endif /* SRC_DEBUG_H_ */
