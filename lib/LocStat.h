/**
 * \file LocStat.h
 * \date 15.01.2017
 * \author: saell
 */

#ifndef LIB_SRC_LOCSTAT_H_
#define LIB_SRC_LOCSTAT_H_

#include <string>

class LocStat
{
    public:
        int lines_All;
        int lines_Code;
        int lines_Empty;
        int lines_Comment;
        bool isBlockComment;

        LocStat(void);
        LocStat(bool verbose);
        virtual ~LocStat();

        int analyseFile(const char* pcPathToFile);
        int newLine(const char* pcLineToCheck);

    private:
        static const int maxLineLenght = 1024;
        bool debugInformation = false;

        void output( bool bIsError, std::string debugInfo );
};

#endif /* LIB_SRC_LOCSTAT_H_ */
