/**
 * \file LocStat.cpp
 * \date 15.01.2017
 * \author saell
 */

#include "LocStat.h"
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <iostream>

LocStat::LocStat( ) :
        lines_All( 0 ), lines_Code( 0 ), lines_Empty( 0 ), 
        lines_Comment( 0 ), isBlockComment( false ), 
        debugInformation(false)
{
    //ctor
}

LocStat::LocStat(bool verbose) :
        lines_All( 0 ), lines_Code( 0 ), lines_Empty( 0 ), 
        lines_Comment( 0 ), isBlockComment( false ), 
        debugInformation(verbose )
{
    //ctor
}

void LocStat::output( bool bIsError, std::string debugInfo )
{
    if (bIsError)
        std::cout<< debugInfo << std::endl;

    if (this->debugInformation)
        std::cout<< debugInfo << std::endl;
}

int LocStat::analyseFile(const char* pcPathToFile)
{
    this->lines_All = 0;
    this->lines_Code = 0;
    this->lines_Empty = 0;
    this->lines_Comment = 0;
    this->isBlockComment = false;

    FILE* pFile = fopen( pcPathToFile, "r" );

    if ( !pFile )
    {
        output(true, std::string("File not Found") + std::string( pcPathToFile ) );
        return -1;
    }
    
    //read line by line and feed to counter
    char line[maxLineLenght];

    while ( fgets( line, sizeof(line), pFile ) )
    {
        /* note that fgets don't strip the terminating \n, checking its presence would allow to handle lines longer that sizeof(line) */
        this->newLine( line );
    }
    /* may check feof here to make a difference between eof and io failure -- network timeout for instance */

    fclose( pFile );

    return 0;
}

LocStat::~LocStat()
{
    //dtor
}

static const char* pcSkipLeadingSpacesAndTabs(const char* pcLineToCheck)
{
    while ( *pcLineToCheck == ' ' || *pcLineToCheck == '\t' ) // spaces/Tabs
        pcLineToCheck++;

    return pcLineToCheck;
}

int removeTrailingWhiteSpaces(char* pcLineToCheck)
{
    int i = 0;
    int lenLineToCheck = strlen(pcLineToCheck);
    char* pcCharToCheck;
    while ( i <= lenLineToCheck )
    {
        pcCharToCheck = &pcLineToCheck[lenLineToCheck - i - 1];
        if ( !isspace( *pcCharToCheck ) && (*pcCharToCheck != '\n') )
        {
            break;
        }
        i++;
    }
    *( pcCharToCheck + 1 )= 0;

    return 0;
}

static bool bIsSingleLineComment(const char* pcLineToCheck)
{
    const char* pcCharacterToCheck = pcSkipLeadingSpacesAndTabs( pcLineToCheck );
    if ( strncmp( pcCharacterToCheck, "//", strlen( "//" ) ) == 0 )
        return true;

    return false;
}

static bool bIsBlockCommentStart(const char* pcLineToCheck)
{
    const char* pcCharacterToCheck = pcSkipLeadingSpacesAndTabs( pcLineToCheck );
    if ( strncmp( pcCharacterToCheck, "/*", strlen( "/*" ) ) == 0 )
        return true;

    return false;
}

static bool bIsBlockCommentEnd(const char* pcLineToCheck)
{
    char pcCharacterToCheck[ strlen( pcLineToCheck ) + 1 ];
    strncpy( pcCharacterToCheck, pcLineToCheck, strlen( pcLineToCheck ) + 1 );

    removeTrailingWhiteSpaces( pcCharacterToCheck );

    if ( strncmp( &pcCharacterToCheck[ strlen( pcCharacterToCheck ) - 2], "*/", strlen( "*/" ) ) == 0 )
        return true;

    return false;
}

static bool bIsSingleLineEmpty(const char* pcLineToCheck)
{
    char pcCharacterToCheck[ strlen( pcLineToCheck ) + 1 ];
    strncpy( pcCharacterToCheck, pcLineToCheck, strlen( pcLineToCheck ) + 1 );

    removeTrailingWhiteSpaces( pcCharacterToCheck );

    if ( strlen( pcCharacterToCheck ) == 0 )
        return true;

    return false;
}

int LocStat::newLine(const char* pcLineToCheck)
{
    //todo check pcLineToCheck is oneSingle Line
    this->lines_All++;

    if ( this->isBlockComment == false && bIsBlockCommentStart( pcLineToCheck ) )
    {
        this->lines_Comment++;
        this->isBlockComment = true;
    }
    else if ( this->isBlockComment && bIsBlockCommentEnd( pcLineToCheck ) )
    {
        this->lines_Comment++;
        this->isBlockComment = false;
    }
    else if ( bIsSingleLineComment( pcLineToCheck ) )
    {
        this->lines_Comment++;
    }
    else if ( bIsSingleLineEmpty( pcLineToCheck ) )
    {
        this->lines_Empty++;
    }
    else if ( this->isBlockComment )
    {
        this->lines_Comment++;
    }
    else
    {
        this->lines_Code++;
        
        for ( unsigned int index = 0; index <= strlen( pcLineToCheck ); index++ )
        {
            if ( (this->isBlockComment == false) && bIsBlockCommentStart( &pcLineToCheck[index] ) )
            {
                this->isBlockComment = true;
            }
            else
            {
            }
        }
    }
    return 0;
}
