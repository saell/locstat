/**
 * \file LocStat_Test.cpp
 * \date 15.01.2017
 * \author: saell
 */

#include "lest.hpp"
#include "LocStat.h"
using namespace std;

const lest::test specification[] =
{
CASE( "Parser handles C and C++ correct" )
{
    SETUP( "handler for Newline")
    {
        LocStat* myLoc = new LocStat();

        EXPECT(myLoc->lines_Code == 0);
        EXPECT(myLoc->lines_Comment == 0);

        SECTION( "SimpleText" )
        {
            myLoc->newLine("text");
            EXPECT(1 == myLoc->lines_Code);
            EXPECT(0 == myLoc->lines_Comment);
        }

        SECTION( "Preprocessor" )
        {
            myLoc->newLine("#ifdef SomePreProDefine");
            EXPECT(1 == myLoc->lines_Code);
            EXPECT(0 == myLoc->lines_Comment);
        }

        SECTION( "IncludeSomeHeader" )
        {
            myLoc->newLine("#include \"SomeInclude.h\"");
            EXPECT(1 == myLoc->lines_Code);
            EXPECT(0 == myLoc->lines_Comment);
        }

        SECTION( "IncludeSystemHeader" )
        {
            myLoc->newLine("#include <SomeInclude.h>");
            EXPECT(1 == myLoc->lines_Code);
            EXPECT(0 == myLoc->lines_Comment);
        }

        SECTION( "someTextWithTabs" )
        {
            myLoc->newLine("\t\t #include <SomeInclude.h>");
            EXPECT(1 == myLoc->lines_Code);
            EXPECT(0 == myLoc->lines_Comment);
        }

        SECTION( "WithOneLineComment" )
        {
            myLoc->newLine("#include <SomeInclude.h> //OneLineComment");
            EXPECT(1 == myLoc->lines_Code);
            EXPECT(0 == myLoc->lines_Comment);
        }

        SECTION( "SingleLineComment" )
        {
            myLoc->newLine("//text");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }

        SECTION( "SingleLineComment_withSpaceBefore" )
        {
            myLoc->newLine(" //#ifdef SomePreProDefine");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }

        SECTION( "SingleLineComment_withMultipleSpacesBefore" )
        {
            myLoc->newLine("   //#include \"SomeInclude.h\"");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }

        SECTION( "SingleLineComment_withMultipleTabsBefore" )
        {
            myLoc->newLine("\t\t\t//#include \"SomeInclude.h\"");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }
    }
},

CASE( "newline handles BlockComments" )
{
    SETUP( "handler for Newline")
    {
        LocStat* myLoc = new LocStat();

        EXPECT(myLoc->lines_Code == 0);
        EXPECT(myLoc->lines_Comment == 0);

        SECTION( "SingleLine" )
        {
            myLoc->newLine("/* Single Line Block Comment*/");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }

        SECTION( "SingleLine_withMultipleSpaceBefore" )
        {
            myLoc->newLine("   /* Single Line Block Comment*/");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }

        SECTION( "SingleLine_withMultipleTabsBefore" )
        {
            myLoc->newLine("\t\t\t/* Single Line Block Comment*/");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
        }

        SECTION( "TwoLines" )
        {
            myLoc->newLine("/* First Line Block Comment");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);

            myLoc->newLine("   Second Line Block Comment*/");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(2 == myLoc->lines_Comment);
        }

        SECTION( "multipleLines" )
        {
            myLoc->newLine("/* Line Block Comment start");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Comment);
            EXPECT(true == myLoc->isBlockComment);

            myLoc->newLine("#include <SomeInclude.h>");
            myLoc->newLine("#include <SomeMoreInclude.h>");
            myLoc->newLine("   Block Comment end*/");

            EXPECT(0 == myLoc->lines_Code);
            EXPECT(4 == myLoc->lines_Comment);
        }

        SECTION( "starts after some code" )
        {
            myLoc->newLine("i++;     /* Line Block Comment start");
            EXPECT( 1 == myLoc->lines_Code );
            EXPECT( 0 == myLoc->lines_Comment );
            EXPECT( true == myLoc->isBlockComment );
        }

        SECTION( "Header Comment is ok" )
        {
            myLoc->newLine("/*\n");
            myLoc->newLine(" * main.cpp\n");
            myLoc->newLine(" *\n");
            myLoc->newLine(" *  Created on: 17.12.2016\n");
            myLoc->newLine(" *      Author: saell\n");
            myLoc->newLine(" */\n");

            EXPECT( 0 == myLoc->lines_Code );
            EXPECT( 6 == myLoc->lines_Comment );
            EXPECT( false == myLoc->isBlockComment );
        }
    }
},

CASE( "newline handles emptylines" )
{
    SETUP( "handler for Newline")
    {
        LocStat* myLoc = new LocStat();

        EXPECT(myLoc->lines_Code == 0);
        EXPECT(myLoc->lines_Comment == 0);

        SECTION( "whitespaces" )
        {
            myLoc->newLine("                 ");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Empty);
        }

        SECTION( "lineBreaks" )
        {
            myLoc->newLine("\n");
            EXPECT(0 == myLoc->lines_Code);
            EXPECT(1 == myLoc->lines_Empty);
        }
    }
}
};

int main(int argc, char * argv[])
{
    return lest::run( specification, argc, argv );
}
