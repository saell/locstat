This project is inspired by some coding katas from [ccd-shool](http://ccd-school.de/coding-dojo/):  

 * [function-katas/loc]( http://ccd-school.de/coding-dojo/function-katas/loc/ ),
 * [application-katas/loc-count]( http://ccd-school.de/coding-dojo/application-katas/loc-count/ ),
 * [architecture-katas/loc-stat]( http://ccd-school.de/coding-dojo/architecture-katas/loc-stat/ )

Why an other code counter?
==========================

My experience up to now was mainly development of embedded systems (c/c++) with "nearly no" resources. With this 
project I aim to get some experience in different fields with no specific order:

workflow / collaboration
------------------------

* getting to know an open source collaboration platform
* collaborating with an (hopefully) active community 
* exercise TDD worklfow
* ...

technical things I like to know better
--------------------------------------

* different build systems ( others than the ones I used to know (make / rake) )
* library building
* paralell processing
* cross plattform development
* best practice for current C++ development 
* ...

How to go on?
=============

* [Getting Started](file:///./GettingStarted.md)
* [Todo's](file:///./Todo.md)