import unittest
import subprocess 
from subprocess import CalledProcessError

cliExecutable = "../src/locstat_cli.exe"
    
class TestCliOptions (unittest.TestCase) :
    def test_option_help_short(self):
        output = subprocess.check_output( [ cliExecutable , "-h" ] )
        self.assertTrue("USAGE: locstat.exe [options] sourcefile" in output )
        
    def test_option_help_long(self):
        output=""
        try:
            output = subprocess.check_output( [ cliExecutable , "--help" ] )
        except CalledProcessError as err:
            print "{0}".format(err)
        self.assertTrue("USAGE: locstat.exe [options] sourcefile" in output )
                         
    def test_option_verbose_short(self):
        output=""
        try:
            output = subprocess.check_output( [ cliExecutable , "-v" ] )
        except CalledProcessError as err:
            print "{0}".format(err)
        self.assertFalse( "USAGE: locstat.exe [options] sourcefile" in output )
                         
    def test_option_verbose_long(self):
        output=""
        try:
            output = subprocess.check_output( [ cliExecutable , "--verbose" ] )
        except CalledProcessError as err:
            print "{0}".format(err)
        self.assertFalse("USAGE: locstat.exe [options] sourcefile" in output )
        

class TestUsageForFiles (unittest.TestCase) :    
    def test_error_File_noneGiven(self):
        #fixme check return code is != 0
        #fixme check stderr gives correct reason        
        output = subprocess.check_output( [ cliExecutable ] )
        self.assertTrue("No File to analyse given" in output)
        self.assertTrue("USAGE: locstat.exe [options] sourcefile" in output )
        
    def test_File_single(self):
        output = subprocess.check_output( [ cliExecutable , "../src/locstat.cpp" ] )
#         print output
        self.assertTrue("../src/locstat.cpp, 122, 94, 9, 19" in output )

    def test_File_doesntExist(self):
        #fixme check return code is != 0
        #fixme check stderr gives correct reason        
        output = subprocess.check_output( [ cliExecutable , "notpresentFile.cpp" ] )
        print output
        self.assertTrue("Can't analyse File:<notpresentFile.cpp>. Does it exist?" in output)
        
    def test_File_two(self):
        output = subprocess.check_output( [ cliExecutable , "../src/locstat.cpp", "../lib/LocStat.cpp" ] )
        #print output 
        
    def test_File_three(self):
        output = subprocess.check_output( [ cliExecutable , "../src/locstat.cpp", "../lib/LocStat.cpp", "../lib/test/LocStat_Test.cpp" ] )
        #print output 
       
class TestUsageForDirectories (unittest.TestCase) :    
    def test_Dir_doesntExist(self):
        #fixme check return code is != 0
        #fixme check stderr gives correct reason        
        output = subprocess.check_output( [ cliExecutable , "./huhu/" ] )
        self.assertTrue("Can't analyse File:<./huhu/>. Does it exist?" in output)
        
    def test_Dir(self):
        output = subprocess.check_output( [ cliExecutable , "../src/" ] )
        #print output 
        
# -----------------  Load all the test cases
if __name__ == '__main__':
    suiteList = []
    suiteList.append(unittest.TestLoader().loadTestsFromTestCase(TestCliOptions))
    suiteList.append(unittest.TestLoader().loadTestsFromTestCase(TestUsageForFiles))
    suiteList.append(unittest.TestLoader().loadTestsFromTestCase(TestUsageForDirectories))
    
    # ----------------   Join them together ane run them
    comboSuite = unittest.TestSuite(suiteList)
    unittest.TextTestRunner(verbosity=0).run(comboSuite)
